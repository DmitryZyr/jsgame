from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from werkzeug import SharedDataMiddleware
import os
from game import Game
import sys
import logging

logger = logging.getLogger()
hdlr = logging.FileHandler('logs/python.log')
formatter = logging.Formatter('backend %(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)


_old_excepthook = sys.excepthook


def myexcepthook(exctype, value, traceback):
    logging.getLogger().exception(value)
    _old_excepthook(exctype, value, traceback)

sys.excepthook = myexcepthook

app = Flask(__name__)
game = Game(app)
app.logger.info("Backend app loaded")


@app.route("/")
def index():
    if request.environ.get('wsgi.websocket'):
        app.logger.info("Serving websocket connection")
        websocket = request.environ['wsgi.websocket']
        app.logger.info("Additing client to game")
        client = game.joinClient(websocket)
        app.logger.info("Waiting client to exit")
        client.join()
        app.logger.info("Closing client connection")
        return "Goodbye :)"
    else:
        return render_template("index.html")


app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
  '/': os.path.join(os.path.dirname(__file__), 'static')
}, cache=False)

if __name__ == '__main__':
    game.start()
    http_server = WSGIServer(('', int(sys.argv[1])), app, handler_class=WebSocketHandler)
    http_server.serve_forever()
