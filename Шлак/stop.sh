#!/bin/sh

echo "Killing gunicorn processes..."

pkill -9 gunicorn
pkill -9 frontend 
pkill -9 backend 
pkill -9 python

echo "Done"
