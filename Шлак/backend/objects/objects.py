import uuid


class GameObject:

    Sprites = {"down": [["player/player_down-1.png", 8], ["player/player_down-2.png", 8], ["player/player_down-3.png", 8]]}

    def __init__(self):
        self.position = {"x": 0, "y": 0}
        self.polygons = []
        self.type = "static"
        self.isBullet = False
        self.allowSleep = True
        self.id = str(uuid.uuid4())
        self.sprite_align = ["center", "center"]

    @staticmethod
    def GetSpritesUrls(obj):
        sprites = []
        for actionFrames in obj.Sprites.values():
            for animationFrame in actionFrames:
                sprites.append("sprites/" + animationFrame[0])
        return sprites

    def serialize(self):
        return self.__dict__
