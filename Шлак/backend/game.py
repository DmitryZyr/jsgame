import gevent
from gevent import Greenlet
import uuid
from objects.environment import Barrel, Ground
from objects.characters import Player
from objects.objects import GameObject
import logging
import inspect
import sys
import types
import json
from Box2D import b2World
from datetime import datetime

log = logging.getLogger()


class Client(Greenlet):

    def __init__(self, socket):
        Greenlet.__init__(self)
        self.id = uuid.uuid1()
        self.socket = socket
        self.running = True
        log.info("Client %s created" % self.id)

    def command(self, command, data):
        command = {"command": command, "data": {}}
        command["data"].update(data)
        self.socket.send(json.dumps(command))

    def messageHandler(self, message):
        pass

    def _run(self):
        while self.running:
            log.info("Waiting client messages")
            try:
                message = self.socket.receive()
                if not message:
                    log.info("None message recieved: closing client")
                    self.running = False
                log.info("Message recieved: %s" % message)
                self.messageHandler(message)
            except Exception, e:
                log.info("Exception: %s" % e)
                self.running = False


class Game(Greenlet):

    FPS = 30
    VelocityIterations = 10
    PositionIterations = 10

    @staticmethod
    def GenerateMap():
        gamemap = []
        for y in range(-16, 16):
            for x in range(-16, 16):
                groundTile = Ground()
                groundTile.position = {"x": x, "y": y}
                gamemap.append(groundTile)
        return gamemap

    def __init__(self, app):
        Greenlet.__init__(self)
        self.app = app
        self.clients = []
        self.map = Game.GenerateMap()

    def SendClientSprites(self, client):
        for module in [k for k in sys.modules.keys() if k.startswith("objects.")]:
            for name, obj in inspect.getmembers(sys.modules[module], inspect.isclass):
                if obj is not types.NoneType:
                    client.command('reciveSpriteUrls', {"urls": GameObject.GetSpritesUrls(obj)})

    def SendClientSomeMap(self, client):
        self.app.logger.info("Sending map to client...")
        for groundtile in self.map:
            client.command("create", groundtile.serialize())

    def onJoin(self, client):
        self.SendClientSprites(client)
        self.SendClientSomeMap(client)

    def joinClient(self, clientWebSocket):
        client = Client(clientWebSocket)
        client.start()
        self.clients.append(client)
        self.app.logger.info("Now serving %i clients" % len(self.clients))
        self.onJoin(client)
        return client

    def _run(self):
        self.app.logger.info("Game greenlet started (%s)" % id(self))
        self.world = b2World(gravity=(0, 0), doSleep=False)

        lastStep = datetime.now()
        while True:
            gevent.sleep(1.0 / Game.FPS)
            now = datetime.now()
            stepTime = float((now - lastStep).microseconds) / 10 ** 6
            self.world.Step(stepTime, Game.VelocityIterations, Game.PositionIterations)
            lastStep = now

        self.app.logger.info("Game greenlet stopping (%s)" % id(self))
