from flask import Flask, redirect, request
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer
from werkzeug.wsgi import SharedDataMiddleware
import os
from game import Game
import sys
import logging

logger = logging.getLogger()
hdlr = logging.FileHandler('../logs/python.log')
formatter = logging.Formatter('backend %(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.DEBUG)


_old_excepthook = sys.excepthook


def myexcepthook(exctype, value, traceback):
    logging.getLogger().exception(value)
    _old_excepthook(exctype, value, traceback)

sys.excepthook = myexcepthook

app = Flask(__name__)
game = Game(app)
game.start()
app.logger.info("Backend app loaded")

@app.route("/")
def index():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        game.joinClient(websocket)
        return "Goodbye :)"
    else:
        return redirect("/index.html")


app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/': os.path.join(os.path.dirname(__file__), '../static')
})

if __name__ == '__main__':
    http_server = WSGIServer(('', 5000), app, handler_class=WebSocketHandler)
    http_server.serve_forever()
