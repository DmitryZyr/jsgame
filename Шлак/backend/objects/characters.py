import uuid
import random
from objects import GameObject


class Player(GameObject):

    Sprites = {
                "down": [["player/player_down-1.png", 8], ["player/player_down-2.png", 8], ["player/player_down-3.png", 8]],
                "left": [["player/player_left-1.png", 8], ["player/player_left-2.png", 8], ["player/player_left-3.png", 8]],
                "right": [["player/player_right-1.png", 8], ["player/player_right-2.png", 8], ["player/player_right-3.png", 8]],
                "up": [["player/player_up-1.png", 8], ["player/player_up-2.png", 8], ["player/player_up-3.png", 8]]
            }

    @classmethod
    def Data(cls):
        return {
            "id": str(uuid.uuid1()),
            "object": "Player",
            "sprites": Player.Sprites,
            "type": "dynamic",
            "polygones": [
                [-5, -5],
                [5, -5],
                [5, 5],
                [5, -5]
            ],
            "heading": 0
        }

    @classmethod
    def create(cls, world, id):
        uData = Player.Data()
        uData["id"] = str(id)
        body = world.CreateDynamicBody(
            position=(random.random() * 800, random.random() * 600),
            allowSleep=False,
            awake=True,
            fixedRotation=True,
            userData=uData
        )
        body.CreatePolygonFixture(
            box=(10, 10),
            density=1,
            friction=0.5
        )
        return body
