#!/bin/sh

mkdir -p logs
echo "Running frontend on 80 port..."
sudo gunicorn --log-file=logs/gunicorn.log -D -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker" frontend.frontend:app -b 0.0.0.0:80
