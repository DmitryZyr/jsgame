from flask import Flask, request, render_template, make_response
from werkzeug import SharedDataMiddleware
import os
import json
from subprocess import call
import gevent
import socket


app = Flask(__name__)
app.logger.info("Game app started")

HOSTNAME = socket.gethostname()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/help")
def help():
    return render_template("help.html")


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/start")
def game():
    if request.environ.get('wsgi.websocket'):
        websocket = request.environ['wsgi.websocket']
        app.logger.info("Starting backend...")
        logfile = open('logs/python.log', 'a')
        call(['./backend/launch-backend.sh', '9099'], stdout=logfile, stderr=logfile)
        gevent.sleep(1)
        app.logger.info("Redirecting user to " + HOSTNAME + ":9099")
        websocket.send(json.dumps({"command": "redirect", "url": "http://" + HOSTNAME + ":9099"}))
        return "Ok"
    else:
        resp = make_response(render_template("start.html"))
        resp.headers['Cache-Control'] = "no-cache"
        return resp

app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
  '/': os.path.join(os.path.dirname(__file__), 'static')
}, cache=False)
